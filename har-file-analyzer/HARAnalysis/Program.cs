﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using HARAnalysis.IpLocationQuery;
using HARAnalysis.Model;
using HtmlAgilityPack;
using Nest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;

namespace HARAnalysis
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var file = args != null && args.Length > 0 ? args[0] : "dell-cn.har";
            var sourceSiteUrl = args != null && args.Length > 1 ? args[1] : string.Empty;
            int lastIndex = file.LastIndexOf(".", StringComparison.Ordinal);
            var targetFile = file.Substring(0, lastIndex);

            if (!File.Exists(file))
            {
                Console.WriteLine("No har file input, will exit...");
                return;
            }

            var settingsFile = "settings.json";
            if (!File.Exists(settingsFile))
            {
                Console.WriteLine("Didn't find settings file, will exit...");
                return;
            }

            var settings = new Settings();
            using (var reader = new StreamReader(settingsFile))
            {
                var settingsStr = reader.ReadToEnd();
                settings = JsonConvert.DeserializeObject<Settings>(settingsStr);
                Console.WriteLine(settings);
            }
            LocationQueryProvider.Instance.Register("api", s => new ApiLocationQuery(s));
            LocationQueryProvider.Instance.Register("spider", s => new SpiderLocationQuery(s));

            StreamReader sr = new StreamReader(file);
            string content = sr.ReadToEnd();
            JObject googleSearch = JObject.Parse(content);

            IList<JToken> results = googleSearch["log"]["entries"].Children().ToList();
            List<AnalysisItem> items = new List<AnalysisItem>();

            foreach (var result in results)
            {
                items.Add(new AnalysisItem
                {
                    Duration = result["time"].ToString(),
                    RequestMethodType = result["request"]["method"].ToString(),
                    RequestUrl = result["request"]["url"].ToString(),
                    ResponseConentType = result["response"]["headers"].Children().ToList().FirstOrDefault(h => h["name"].ToString() == "Content-Type")?["value"].ToString(),
                    RequestHostName = result["request"]["headers"].Children().ToList().FirstOrDefault(h => h["name"].ToString() == "Host")?["value"].ToString(),
                    ResponseStatus = result["response"]["status"].ToString(),
                    ServerIpAddress = result["serverIPAddress"].ToString()

                });
            }
            Console.WriteLine(items.Count);



            List<AnalysisResult> analysisResults = new List<AnalysisResult>();

            foreach (var analysisItem in items)
            {
                var queryResult = QueryIPLocation(analysisItem, settings);

                var result = new AnalysisResult
                {
                    AnalysisItem = analysisItem,
                    Country = queryResult.Country,
                    Region =  queryResult.Region,
                    Host = queryResult.Host,
                    ISP = queryResult.ISP,
                    Latitude = queryResult.Latitude,
                    Longitude = queryResult.Longitude
                };
                analysisResults.Add(result);
                Console.WriteLine($"HTTP PARSER: {analysisResults.Count}");
                Thread.Sleep(1000);
            }

            WriteToElastic(settings, analysisResults, sourceSiteUrl);

            Console.WriteLine("Begin Excel Processing.");
            var newFile = $"{targetFile}.xlsx";

            using (var fs = new FileStream(newFile, FileMode.Create, FileAccess.Write))
            {
                IWorkbook workbook = new XSSFWorkbook();
                ISheet sheet1 = workbook.CreateSheet("Sheet1");
                var rowIndex = 0;
                IRow row = sheet1.CreateRow(rowIndex);
                //row.Height = 30 * 80;
                row.CreateCell(0).SetCellValue("RequestUrl");
                row.CreateCell(1).SetCellValue("RequestMethodType");
                row.CreateCell(2).SetCellValue("Duration");
                row.CreateCell(3).SetCellValue("ResponseStatus");
                row.CreateCell(4).SetCellValue("ResponseConentType");
                row.CreateCell(5).SetCellValue("RequestHostName");
                row.CreateCell(6).SetCellValue("ServerIpAddress");
                row.CreateCell(7).SetCellValue("Host");
                row.CreateCell(8).SetCellValue("ISP");
                row.CreateCell(9).SetCellValue("Country");
                foreach (var analysisResult in analysisResults)
                {
                    rowIndex++;
                    IRow rowRow = sheet1.CreateRow(rowIndex);
                    rowRow.CreateCell(0).SetCellValue(analysisResult.AnalysisItem.RequestUrl);
                    rowRow.CreateCell(1).SetCellValue(analysisResult.AnalysisItem.RequestMethodType);
                    rowRow.CreateCell(2).SetCellValue(analysisResult.AnalysisItem.Duration);
                    rowRow.CreateCell(3).SetCellValue(analysisResult.AnalysisItem.ResponseStatus);
                    rowRow.CreateCell(4).SetCellValue(analysisResult.AnalysisItem.ResponseConentType);
                    rowRow.CreateCell(5).SetCellValue(analysisResult.AnalysisItem.RequestHostName);
                    rowRow.CreateCell(6).SetCellValue(analysisResult.AnalysisItem.ServerIpAddress);
                    rowRow.CreateCell(7).SetCellValue(analysisResult.Host);
                    rowRow.CreateCell(8).SetCellValue(analysisResult.ISP);
                    rowRow.CreateCell(9).SetCellValue(analysisResult.Country);
                }
                workbook.Write(fs);
            }

            Console.WriteLine("Complete Excel Processed.");
            Thread.Sleep(10000);
        }

        private static IpLocationQueryResult QueryIPLocation(AnalysisItem analysisItem, Settings settings)
        {
            var query = LocationQueryProvider.Instance.GetLocationQuery(settings.IpQueryMethod, settings);
            if (query == null)
            {
                throw new ArgumentException("cannot find matched query...");
            }

            return query.Query(analysisItem, settings);
        }

        static void WriteToElastic(Settings settings, List<AnalysisResult> results, string siteUrl)
        {
            Console.WriteLine("Begin to write to Elastic-Search ...");
            try
            {
                if (string.IsNullOrEmpty(settings.ElasticIndex))
                {
                    Console.WriteLine("Didn't provide any elastic index...");
                    return;
                }
                var node = new Uri(settings.ElasticUri);
                var esSettings = new ConnectionSettings(node);

                var client = new ElasticClient(esSettings);
                if (!client.IndexExists(settings.ElasticIndex).Exists)
                {
                    Console.WriteLine($"Did not exist index '{settings.ElasticIndex}', creating ...");
                    client.CreateIndex(settings.ElasticIndex,
                        c => c.Mappings(m => m.Map<AnalysisElasticItem>(mm => mm.AutoMap())));
                    Console.WriteLine($"'{settings.ElasticIndex}', created ...");
                }

                foreach (var source in results)
                {
                    var esItem = new AnalysisElasticItem();
                    esItem.FromAnalysisResult(source);
                    esItem.SiteUrl = siteUrl;
                    var response = client.Index(esItem, idx => idx.Index(settings.ElasticIndex));
                    Console.WriteLine($"Response from Elastic Server: {response}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.WriteLine("Finished to write to Elastic-Search ...");
        }
    }
}
