﻿using System;
using System.Collections.Generic;
using System.Text;
using HARAnalysis.Model;

namespace HARAnalysis
{
    public interface IHarReader
    {
        IList<AnalysisItem> ReadItems();
    }
}
