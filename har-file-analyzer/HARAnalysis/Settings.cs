﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HARAnalysis
{
    public class Settings
    {
        public bool Proxy { get; set; }
        public string ProxyUrl { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ElasticUri { get; set; }
        public string ElasticIndex { get; set; }
        public string IpQueryMethod { get; set; }
    }
}
