﻿using System;
using System.Collections.Generic;
using System.Text;
using HARAnalysis.Model;

namespace HARAnalysis.IpLocationQuery
{
    public interface ILocationQuery
    {
        IpLocationQueryResult Query(AnalysisItem analysisItem, Settings settings);
    }
}
