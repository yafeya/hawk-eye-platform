﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using HARAnalysis.Model;
using HtmlAgilityPack;

namespace HARAnalysis.IpLocationQuery
{
    public class SpiderLocationQuery : ILocationQuery
    {
        public SpiderLocationQuery(Settings settings)
        {
        }

        public IpLocationQueryResult Query(AnalysisItem analysisItem, Settings settings)
        {
            var result = new IpLocationQueryResult();
            var html = $@"https://ipsnoop.com/{analysisItem.ServerIpAddress}";

            WebProxy proxy = string.IsNullOrEmpty(settings.ProxyUrl) ? null : new WebProxy(new Uri(settings.ProxyUrl));
            if (settings.Proxy && proxy != null)
            {
                proxy.UseDefaultCredentials = false;
                proxy.Credentials = new NetworkCredential(settings.UserName, settings.Password);
            }

            HtmlWeb web = new HtmlWeb
            {
                PreRequest = delegate (HttpWebRequest webRequest)
                {
                    webRequest.Timeout = 1500000;
                    if (settings.Proxy && proxy != null)
                    {
                        webRequest.Proxy = proxy;
                    }

                    return true;
                }
            };

            var htmlDoc = web.Load(html);

            var node = htmlDoc.DocumentNode.SelectSingleNode("//div[@class='res']");
            foreach (var childNode in node.ChildNodes)
            {
                //Console.WriteLine(childNode.InnerHtml);
                if (childNode.InnerHtml.Trim() == "Host:")
                {
                    result.Host = childNode.NextSibling.InnerHtml;
                }
                else if (childNode.InnerHtml.Trim() == "ISP:")
                {
                    result.ISP = childNode.NextSibling.InnerHtml;
                }
                else if (childNode.InnerHtml.Trim() == "Country:")
                {
                    result.Country = childNode.NextSibling.InnerHtml;
                }
                else if (childNode.InnerHtml.Trim() == "Region:")
                {
                    result.Region = childNode.NextSibling.InnerHtml;
                }
                else if (childNode.InnerHtml.Trim() == "Latitude:")
                {
                    result.Latitude = double.Parse(childNode.NextSibling.InnerHtml);
                }
                else if (childNode.InnerHtml.Trim() == "Longitude:")
                {
                    result.Longitude = double.Parse(childNode.NextSibling.InnerHtml);
                }
            }

            return result;
        }
    }
}
