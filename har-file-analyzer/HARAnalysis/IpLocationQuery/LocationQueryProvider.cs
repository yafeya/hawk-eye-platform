﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using Nest;

namespace HARAnalysis.IpLocationQuery
{
    public interface ILocationQueryProvider
    {
        ILocationQueryProvider Register(string name, Func<Settings, ILocationQuery> creator);
        ILocationQuery GetLocationQuery(string name, Settings settings);
    }

    public class LocationQueryProvider : ILocationQueryProvider
    {
        private static ILocationQueryProvider mInstance = new LocationQueryProvider();

        private ConcurrentDictionary<string, Func<Settings, ILocationQuery>> mInnerDictionary =
            new ConcurrentDictionary<string, Func<Settings, ILocationQuery>>();

        private LocationQueryProvider()
        {
        }

        public static ILocationQueryProvider Instance => mInstance;

        public ILocationQueryProvider Register(string name, Func<Settings, ILocationQuery> creator)
        {
            mInnerDictionary.AddOrUpdate(name, creator, (k, v) => creator);
            return this;
        }

        public ILocationQuery GetLocationQuery(string name, Settings settings)
        {
            ILocationQuery query = null;
            if (mInnerDictionary.ContainsKey(name))
            {
                query = mInnerDictionary[name](settings);
            }

            return query;
        }
    }
}
