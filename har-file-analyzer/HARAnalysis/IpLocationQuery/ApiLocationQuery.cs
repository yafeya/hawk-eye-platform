﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using HARAnalysis.Model;
using Newtonsoft.Json;

namespace HARAnalysis.IpLocationQuery
{
    public class ApiLocationQuery : ILocationQuery
    {
        private HttpClient client = new HttpClient();

        public ApiLocationQuery(Settings settings)
        {
            client.Timeout = TimeSpan.FromSeconds(60);
            if (settings.Proxy)
            {
                var proxy = new WebProxy(new Uri(settings.ProxyUrl));
                proxy.UseDefaultCredentials = false;
                proxy.Credentials = new NetworkCredential(settings.UserName, settings.Password);
                var handler = new HttpClientHandler();
                handler.Proxy = proxy;
                client = new HttpClient(handler);
            }
        }

        public IpLocationQueryResult Query(AnalysisItem analysisItem, Settings settings)
        {
            var result = new IpLocationQueryResult();

            try
            {
                var response = client.GetStringAsync($"http://ip-api.com/json/{analysisItem.ServerIpAddress}").Result;

                var locationResponse = JsonConvert.DeserializeObject<ApiLocationQueryResponse>(response);
                result.Host = locationResponse.Query;
                result.Region = locationResponse.City;
                result.Country = locationResponse.Country;
                result.ISP = $"{locationResponse.Isp} {locationResponse.As}";
                result.Latitude = locationResponse.Lat;
                result.Longitude = locationResponse.Lon;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Get Exception when query ip location, will go on. \n${e.Message}");
            }

            return result;
        }
    }

    class ApiLocationQueryResponse
    {
        public string Status { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Region { get; set; }
        public string RegionName { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string Timezone { get; set; }
        public string Isp { get; set; }
        public string Org { get; set; }
        public string As { get; set; }
        public string Query { get; set; }
    }
}
