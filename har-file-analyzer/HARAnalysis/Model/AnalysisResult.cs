﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HARAnalysis.Model
{
   public  class AnalysisResult
    {
        public AnalysisItem AnalysisItem { get; set; }

        public string Host { get; set; }

        public string ISP { get; set; }

        public string Country { get; set; }

        public string Region { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }
    }
}
