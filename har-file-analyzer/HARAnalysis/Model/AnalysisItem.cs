﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HARAnalysis.Model
{
    public class AnalysisItem
    {
        public string RequestUrl { get; set; }

        public string RequestMethodType { get; set; }

        public string Duration { get; set; }

        public string ResponseStatus { get; set; }

        public string ResponseConentType { get; set; }

        public string RequestHostName { get; set; }

        public string ServerIpAddress { get; set; }
    }
}
