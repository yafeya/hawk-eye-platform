var puppeteer = require('puppeteer');
var PuppeteerHar = require('puppeteer-har');

function harGenerator() {}

harGenerator.prototype.generateHar = function(url, harfile, callback) {
    (async() => {
        var browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox', '--disable-setuid-sandbox'] });
        var page = await browser.newPage();

        var har = new PuppeteerHar(page);
        await har.start({ path: harfile });

        await page.goto(url, { timeout: 120000 });

        await har.stop();
        await browser.close();

        if (callback !== undefined) {
            callback(harfile);
        }
    })();
}

module.exports = harGenerator;