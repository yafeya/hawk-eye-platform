var harGenerator = require('./har-generator');
const fs = require('fs');
const { spawn } = require('child_process')

let rawdata = fs.readFileSync('sites.json');
let sites = JSON.parse(rawdata);
let currentIndex = 0;

function writeHarfile(url, harfile) {
    var generator = new harGenerator();

    console.log(`start to write to ${harfile} ...`);
    generator.generateHar(url, harfile, file => {
        console.log(`finished to write to ${file}`);

        let child = spawn('dotnet', ['HARAnalysis.dll', harfile, url]);

        child.stdout.on('data', (data) => {
            console.log(`child stdout:\n${data}`);
        });

        child.stderr.on('data', (data) => {
            console.error(`child stderr:\n${data}`);
        });

        child.on('exit', code => {
            console.log(`Exit code is: ${code}`);
            currentIndex++;
            generateHarFile();
        });
    });
}

// for (var site of sites) {
//     var harfile = `${site.name}.har`;
//     var url = site.url;
//     writeHarfile(url, harfile);
// }
function generateHarFile() {
    if (currentIndex < sites.length) {
        let site = sites[currentIndex];
        var harfile = `${site.name}.har`;
        var url = site.url;
        writeHarfile(url, harfile);
    }
}

generateHarFile();