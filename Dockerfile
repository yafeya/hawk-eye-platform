FROM yafeya/ubuntu-dotnetcore

RUN mkdir /var/har-file-analyzer/
WORKDIR /var/har-file-analyzer/

COPY ./har-file-analyzer/ ./
RUN ls
WORKDIR HARAnalysis
RUN ls
RUN dotnet build

RUN apt-get update -y
RUN apt-get install --reinstall systemd -y
RUN apt-get install -y curl
RUN apt-get install -y nodejs npm
RUN npm config set registry https://registry.npmjs.org
RUN npm install n -g
RUN n latest

RUN node --version
RUN npm  --version

RUN mkdir /var/har-file-generator
WORKDIR /var/har-file-generator

RUN pwd

RUN apt-get update && apt-get install -y wget --no-install-recommends \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge --auto-remove -y curl \
    && rm -rf /src/*.deb

COPY ./har-file-generator/har-generator.js ./
COPY ./har-file-generator/index.js ./
COPY ./har-file-generator/package.json ./
COPY ./har-file-generator/sites.json ./

RUN cp /var/har-file-analyzer/HARAnalysis/bin/Debug/netcoreapp2.0/HARAnalysis.* ./
RUN cp /var/har-file-analyzer/HARAnalysis/bin/Debug/netcoreapp2.0/settings.json ./

RUN ls

RUN npm install

RUN chmod u+x ./**/*

#ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.0/dumb-init_1.2.0_amd64 /usr/local/bin/dumb-init
#RUN chmod +x /usr/local/bin/dumb-init

#RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
#    && mkdir -p /home/pptruser/Downloads \
#    && chown -R pptruser:pptruser /home/pptruser \
#    && chown -R pptruser:pptruser ./**/*

#USER pptruser


CMD ["node", "index.js"]


